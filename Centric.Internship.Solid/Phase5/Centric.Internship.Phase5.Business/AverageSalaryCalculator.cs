﻿namespace Centric.Internship.Phase5.Business
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Centric.Internship.Phase5.Models;

    public class AverageSalaryCalculator
    {
        public Results CalculateAverageSalary(Employees employees)
        {
            // calculate the average salary
            var salaries = this.GetAllSalaries(employees);

            var results = new Results
            {
                AverageSalary = this.GetAverageSalary(salaries),
                AverageSalaryPerYears = this.GetAverageSalariesPerYear(salaries)
            };

            return results;
        }

        public Results CalculateAverageSalaryForEmployees(Func<Employees, Employees> func, Employees employees)
        {
            return this.CalculateAverageSalary(func(employees));
        }

        private List<Salary> GetAllSalaries(Employees employees)
        {
            var salaries = new List<Salary>();

            foreach (var employee in employees.EmployeesCollection)
            {
                salaries.AddRange(employee.Salaries);
            }

            return salaries;
        }

        private double GetAverageSalary(List<Salary> salaries)
        {
            var employeesCount = salaries.Count;

            return salaries.Sum(x => x.Amount) / employeesCount;
        }

        private AverageSalaryPerYear[] GetAverageSalariesPerYear(List<Salary> salaries)
        {
            var averageSalaryPerYears = salaries
                .GroupBy(x => x.Year)
                .Select(x => new AverageSalaryPerYear
                {
                    Year = x.Key,
                    AverageSalary = x.Sum(y => y.Amount) / x.Count()
                });

            return averageSalaryPerYears.ToArray();
        }
    }
}
