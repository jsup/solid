﻿namespace Centric.Internship.Phase5.Business
{
    using System;

    using Centric.Internship.Phase5.Interfaces;
    using Centric.Internship.Phase5.Models;

    public class SalaryCalculator : ISalaryCalculator
    {
        private IReaderService _fileReaderService;

        private IOutputFileManager _outputFileManager;

        private AverageSalaryCalculator _averageSalaryCalculator;

        public SalaryCalculator(IReaderService fileReaderService, IOutputFileManager outputFileManager)
        {
            this._fileReaderService = fileReaderService;
            this._outputFileManager = outputFileManager;
            this._averageSalaryCalculator = new AverageSalaryCalculator();
        }

        public void CalculateAverageSalary()
        {
            var employees = this._fileReaderService.Read();

            var results = this._averageSalaryCalculator.CalculateAverageSalary(employees);

            this._outputFileManager.OutputResults(results, "ResultsPerYear");
        }

        public void CalculateAverageSalaryForEmployees(Func<Employees, Employees> filter)
        {
            var employees = this._fileReaderService.Read();
            
            var results = this._averageSalaryCalculator.CalculateAverageSalaryForEmployees(filter, employees);

            this._outputFileManager.OutputResults(results, "OperationResultsPerUsers");
        }
    }
}
