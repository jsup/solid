﻿namespace Centric.Internship.Phase5.Models
{
    using System;
    using System.Xml.Serialization;

    [Serializable()]
    [XmlRoot("Results")]
    public class Results
    {
        [XmlElement(ElementName = "AverageSalaryPerYears")]
        public AverageSalaryPerYear[] AverageSalaryPerYears { get; set; }

        [XmlElement(ElementName = "AverageSalary")]
        public double AverageSalary { get; set; }
    }
}