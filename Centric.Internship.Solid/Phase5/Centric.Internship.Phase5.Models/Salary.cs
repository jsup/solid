﻿namespace Centric.Internship.Phase5.Models
{
    using System;
    using System.Xml.Serialization;

    [Serializable()]
    [XmlType("salary")]
    public class Salary
    {
        [XmlElement(ElementName = "year")]
        public string Year { get; set; }

        [XmlElement(ElementName = "amount")]
        public double Amount { get; set; }
    }
}