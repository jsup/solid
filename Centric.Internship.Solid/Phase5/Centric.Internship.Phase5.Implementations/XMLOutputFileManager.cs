﻿namespace Centric.Internship.Phase5.Implementations
{
    using System.IO;
    using System.Xml.Serialization;

    using Centric.Internship.Phase5.Business;
    using Centric.Internship.Phase5.Models;

    public class XmlOutputFileManager : IOutputFileManager
    {
        public void OutputResults(Results results, string fileName)
        {
            var xmlSerializer = new XmlSerializer(typeof(Results));

            using (var fileStream = new FileStream($@"..\..\..\..\{fileName}.xml", FileMode.Create))
            {
                xmlSerializer.Serialize(fileStream, results);
            }
        }
    }
}