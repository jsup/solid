﻿namespace Centric.Internship.Phase5.Implementations
{
    using System.Collections.Generic;
    using System.Net.Http;

    using Centric.Internship.Phase5.Interfaces;
    using Centric.Internship.Phase5.Models;

    public class UrlReaderService : IReaderService
    {
        private string _location;

        private IList<IContentParser> _contentParsers;

        public UrlReaderService()
        {
            this._contentParsers = new List<IContentParser>();
        }

        public Employees Read()
        {
            var httpClient = new HttpClient();
            
            var request = httpClient.GetAsync(this._location).Result;

            var response = request.Content.ReadAsStringAsync().Result;

            foreach (var parser in this._contentParsers)
            {
                if (parser.CanProcess(response))
                {
                    return parser.Process(response);
                }
            }

            return null;
        }

        public void RegisterFileReader(IContentParser parser)
        {
            this._contentParsers.Add(parser);
        }

        public void SetLocation(string location)
        {
            this._location = location;
        }
    }
}
