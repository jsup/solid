﻿namespace Centric.Internship.Phase5.App
{
    using System.Linq;

    using Centric.Internship.Phase5.Business;
    using Centric.Internship.Phase5.Interfaces;
    using Centric.Internship.Phase5.Models;

    public class SalaryApp
    {
        public void Run(IReaderService readerServices, IOutputFileManager outputFileManager)
        {
            var salaryCalculator = new SalaryCalculator(readerServices, outputFileManager);

            salaryCalculator.CalculateAverageSalaryForEmployees(CalculateSalaryForCertainUsers);
        }

        private static Employees CalculateSalaryForCertainUsers(Employees employees)
        {
            employees.EmployeesCollection = employees.EmployeesCollection.Where(x => x.Name.Contains('e')).ToArray();

            return employees;
        }
    }
}
