﻿namespace Centric.Internship.Phase5.Business
{
    using Centric.Internship.Phase5.Models;

    public interface IOutputFileManager
    {
        void OutputResults(Results results, string fileName);
    }
}