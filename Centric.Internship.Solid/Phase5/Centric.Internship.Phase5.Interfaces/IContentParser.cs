﻿namespace Centric.Internship.Phase5.Interfaces
{
    using Centric.Internship.Phase5.Models;

    public interface IContentParser
    {
        bool CanProcess(string content);
        Employees Process(string content);
    }
}
