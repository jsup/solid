﻿using Centric.Internship.Phase3.Interfaces;
using Centric.Internship.Phase3.Models;
using System;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Centric.Internship.Phase3.Implementations
{
    public class XMLFileReader : IFileReader
    {
        public bool CanRead(string filePath)
        {
            try
            {
                XDocument xmlDocument = new XDocument();
                xmlDocument = XDocument.Load(filePath);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Employees Read(string filePath)
        {
            Employees employees = null;

            // read & parse from an xml file
            var serializer = new XmlSerializer(typeof(Employees));

            using (var reader = new StreamReader(filePath))
            {
                employees = (Employees)serializer.Deserialize(reader);
            }

            return employees;
        }
    }
}
