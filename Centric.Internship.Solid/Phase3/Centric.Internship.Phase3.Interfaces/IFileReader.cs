﻿using Centric.Internship.Phase3.Models;

namespace Centric.Internship.Phase3.Interfaces
{
    public interface IFileReader
    {
        bool CanRead(string filePath);
        Employees Read(string filePath);
    }
}
