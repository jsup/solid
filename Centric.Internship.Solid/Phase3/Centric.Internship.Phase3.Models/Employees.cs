﻿using System.Xml.Serialization;

namespace Centric.Internship.Phase3.Models
{
    [XmlRoot("Employees")]
    public class Employees
    {
        [XmlElement("Employee")]
        public Employee[] EmployeesCollection { get; set; }
    }
}