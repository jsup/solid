﻿using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Centric.Internship.Phase1
{
    public class SalaryCalculator
    {
        public void CalculateAverageSalary()
        {
            Employees employees;
            
            // read & parse from an xml file
            var serializer = new XmlSerializer(typeof(Employees));

            using (var reader = new StreamReader((@"..\..\..\..\Salaries.xml")))
            {
                employees = (Employees)serializer.Deserialize(reader);
            }

            // calculate the average salary
            var average = employees.EmployeesCollection.ToList().Sum(x => x.Salary) / employees.EmployeesCollection.Length;

            var results = new Results
            {
                AverageSalary = average
            };

            // output the result in another xml file
            var xmlSerializer = new XmlSerializer(typeof(Results));
            using (var fileStream = new FileStream(@"..\..\..\..\Results.xml", FileMode.Create))
            {
                xmlSerializer.Serialize(fileStream, results);
            }
        }
    }
}
