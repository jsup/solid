﻿using System;
using System.Xml.Serialization;

namespace Centric.Internship.Phase1
{
    [Serializable()]   
    public class Employee
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "salary")]
        public long Salary{ get; set; }
    }
}   