﻿using Centric.Internship.Phase2.Models;

namespace Centric.Internship.Phase2.Interfaces
{
    public interface IEmployeesReader
    {
        Employees Read(string filePath);
    }
}
