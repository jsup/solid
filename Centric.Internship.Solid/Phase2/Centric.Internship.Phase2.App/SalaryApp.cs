﻿using Centric.Internship.Phase2.Business;
using Centric.Internship.Phase2.Implementations;
using System;

namespace Centric.Internship.Phase2.App
{
    public class SalaryApp
    {
        public void Run()
        {
            var salaryCalculator = new SalaryCalculator(new XmlEmployeesReader());

            salaryCalculator.CalculateAverageSalary();
        }
    }
}
