﻿using Centric.Internship.Phase2.Interfaces;
using Centric.Internship.Phase2.Models;
using System.IO;
using System.Xml.Serialization;

namespace Centric.Internship.Phase2.Implementations
{
    public class XmlEmployeesReader : IEmployeesReader
    {
        public Employees Read(string filePath)
        {
            Employees employees;

            // read & parse from an xml file
            var serializer = new XmlSerializer(typeof(Employees));

            using (var reader = new StreamReader(filePath))
            {
                employees = (Employees)serializer.Deserialize(reader);
            }

            return employees;
        }
    }
}
