﻿using System;
using System.Xml.Serialization;

namespace Centric.Internship.Phase2.Models
{
    [Serializable()]
    [XmlRoot("Results")]
    public class Results
    {
        [XmlElement(ElementName = "AverageSalaryPerYears")]
        public AverageSalaryPerYear[] AverageSalaryPerYears { get; set; }

        [XmlElement(ElementName = "AverageSalary")]
        public double AverageSalary { get; set; }
    }
}