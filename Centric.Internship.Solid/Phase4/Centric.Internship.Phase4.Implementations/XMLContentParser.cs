﻿namespace Centric.Internship.Phase4.Implementations
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    using Centric.Internship.Phase4.Interfaces;
    using Centric.Internship.Phase4.Models;

    public class XMLContentParser : IContentParser
    {
        public bool CanProcess(string content)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(content);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Employees Process(string content)
        {
            Employees employees = null;

            // read & parse from an xml file
            var serializer = new XmlSerializer(typeof(Employees));

            using (var reader = new StringReader(content))
            {
                employees = (Employees)serializer.Deserialize(reader);
            }

            return employees;
        }
    }
}
