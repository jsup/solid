﻿namespace Centric.Internship.Phase4.Implementations
{
    using System;

    using Centric.Internship.Phase4.Interfaces;
    using Centric.Internship.Phase4.Models;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class JSONContentParser : IContentParser
    {
        public bool CanProcess(string content)
        {
            try
            {
                var jobj = JObject.Parse(content);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Employees Process(string content)
        {
            Employees employees = null;

            employees = JsonConvert.DeserializeObject<Employees>(content);

            return employees;
        }
    }
}
