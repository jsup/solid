﻿using Centric.Internship.Phase4.Interfaces;
using Centric.Internship.Phase4.Models;
using Newtonsoft.Json;
using System.IO;
using System.Xml.Serialization;

namespace Centric.Internship.Phase4.Business
{
    public class SalaryCalculator
    {
        private IReaderService _fileReaderService;
        private AverageSalaryCalculator _averageSalaryCalculator;

        public SalaryCalculator(IReaderService fileReaderService)
        {
            _fileReaderService = fileReaderService;
            _averageSalaryCalculator = new AverageSalaryCalculator();
        }

        public void CalculateAverageSalary()
        {
            var employees = _fileReaderService.Read();

            var results = _averageSalaryCalculator.CalculateAverageSalary(employees);

            this.OutputResults(results);
        }

        private void OutputResults(Results results)
        {
            var xmlSerializer = new XmlSerializer(typeof(Results));
            using (var fileStream = new FileStream(@"..\..\..\..\ResultsPerYear.xml", FileMode.Create))
            {
                xmlSerializer.Serialize(fileStream, results);
            }
        }
    }
}
