﻿namespace Centric.Internship.Phase4.Interfaces
{
    using Centric.Internship.Phase4.Models;

    public interface IReaderService
    {
        void SetLocation(string location);

        void RegisterFileReader(IContentParser parser);
       
        Employees Read();
    }
}
